-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 05, 2021 at 07:41 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajaxform`
--

-- --------------------------------------------------------

--
-- Table structure for table `costsheet`
--

CREATE TABLE `costsheet` (
  `id` int(9) NOT NULL,
  `area` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `carpet` varchar(255) NOT NULL,
  `agrCost` varchar(255) NOT NULL,
  `mseb` varchar(255) NOT NULL,
  `society` varchar(255) NOT NULL,
  `club` varchar(255) NOT NULL,
  `stamp` varchar(255) NOT NULL,
  `maintenance` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `costsheet`
--

INSERT INTO `costsheet` (`id`, `area`, `rate`, `carpet`, `agrCost`, `mseb`, `society`, `club`, `stamp`, `maintenance`) VALUES
(1, '20*30', '1000', '10*40', '2000.8999', '12345', 'test demo status', '1234.76', '1298.78', '123457');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(9) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varbinary(255) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `name`, `image`, `created_at`) VALUES
(1, 'mahadev ingawale', 0x30636336373233336435323832373837323563336465363738643762636534372e6a7067, '2021-03-05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costsheet`
--
ALTER TABLE `costsheet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `costsheet`
--
ALTER TABLE `costsheet`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
