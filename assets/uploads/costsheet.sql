-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2020 at 07:28 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajaxform`
--

-- --------------------------------------------------------

--
-- Table structure for table `costsheet`
--

CREATE TABLE `costsheet` (
  `id` int(9) NOT NULL,
  `Area` bigint(100) NOT NULL,
  `Rate` bigint(100) NOT NULL,
  `Carpet_Area` float NOT NULL,
  `Agr_Cost` bigint(250) NOT NULL,
  `MSEB` bigint(250) NOT NULL,
  `Society_Formation` bigint(250) NOT NULL,
  `Club_House` bigint(250) NOT NULL,
  `Stamp_Duty` bigint(250) NOT NULL,
  `Maintenance` varchar(250) NOT NULL,
  `Registration` varchar(100) NOT NULL,
  `GST` varchar(250) NOT NULL,
  `Total_Cost` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `costsheet`
--

INSERT INTO `costsheet` (`id`, `Area`, `Rate`, `Carpet_Area`, `Agr_Cost`, `MSEB`, `Society_Formation`, `Club_House`, `Stamp_Duty`, `Maintenance`, `Registration`, `GST`, `Total_Cost`) VALUES
(4, 200, 222, 333, 44444, 444, 333, 444, 4322, '2222', '8765', '3456789', '123456777');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costsheet`
--
ALTER TABLE `costsheet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `costsheet`
--
ALTER TABLE `costsheet`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
