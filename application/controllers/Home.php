<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->view('dashboard');
	}

	public function costsheet()
	{
		$this->load->library('form_validation');
		$this->load->view('costsheet');		
	}
}
