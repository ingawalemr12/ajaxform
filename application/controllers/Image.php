<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller {

	public function imageSelect()
	{	
		$this->load->helper('common_helper');

		$config['upload_path']          = './assets/uploads/category/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']          = TRUE;
		$this->load->library('upload', $config);

		$this->load->model('Imagemodel');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">', '</p>');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		if ($this->form_validation->run() == true) {
			
			//print_r($_FILES['image']); exit();    //or  print_r($_FILES); exit;
			if (!empty($_FILES['image']['name'])) // Now user has selected a file 
			{
				if ($this->upload->do_upload('image'))
				{
					// upload images code
				  $data = $this->upload->data();//echo "<pre>";print_r($data);echo "</pre>"; exit();
				
				// Resize images
				resizeImage($config['upload_path'].$data['file_name'], 
							$config['upload_path'].'thumb/'.$data['file_name'], 300, 200);

			  //resizeImage($config['upload_path'].$data['file_name'], 										  $config['upload_path'].'thumb/'.$data['file_name'], 300, 270);

				  $formArray['image'] = $data['file_name'];
				  $formArray['name'] = $this->input->post('name');
			      $this->Imagemodel->createImage($formArray);
				  $this->session->set_flashdata('success', 'record added');
				  redirect(base_url().'Home/index');
				}
				else
				{
					//show error, if we selected PDF file 
					$error = $this->upload->display_errors('<p class="invalid-feedback">', '</p>');
					$data['erronImage'] = $error;
					$this->load->view('images', $data);
				}
			}						
		}
		else
		{
			$this->load->view('images');
		}		
	}
}
