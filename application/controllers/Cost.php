<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost extends CI_Controller {
	
	public function costCreate()
		{
		$this->load->library('form_validation');
        $this->load->model('Costmodel');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">', '</p>');
		$this->form_validation->set_rules('area', 'Area', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('carpet', 'Carpet Area', 'trim|required');
		$this->form_validation->set_rules('agrCost', 'Agr Cost', 'trim|required');
		$this->form_validation->set_rules('mseb', 'MSEB', 'trim|required');
		$this->form_validation->set_rules('society', 'Society Formation', 'trim|required');
		$this->form_validation->set_rules('club', 'Club House Charges', 'trim|required');
		$this->form_validation->set_rules('stamp', 'Stamp-Duty', 'trim|required');
		$this->form_validation->set_rules('maintenance', 'Maintenance', 'trim|required');
		
			if ($this->form_validation->run() == TRUE)
			{
				//echo "code success..." ;

				$data=array();
				$data['area'] = $this->input->post('area');//$data[''] = $this->input->post('');
				$data['rate'] = $this->input->post('rate');
				$data['carpet'] = $this->input->post('carpet');
				$data['agrCost'] = $this->input->post('agrCost');
				$data['mseb'] = $this->input->post('mseb');
				$data['society'] = $this->input->post('society');
				$data['club'] = $this->input->post('club');
				$data['stamp'] = $this->input->post('stamp');
				$data['maintenance'] = $this->input->post('maintenance');
				

				$this->Costmodel->createSheet($data);
				$this->session->set_flashdata('success', 'Data inserted');
				redirect(base_url().'Cost/getCost');
			
			}
			else
			{
				$this->load->view('costsheet');	
			}
		}
	public function getCost($page=1)
	{ 
		$this->load->model('Costmodel'); 
		$this->load->library('pagination');

		$config['base_url'] = base_url('Cost/getCost');
		$config['total_rows'] = $this->Costmodel->getCostsheetCount();
		$config['per_page'] = 4;
		$config['use_page_numbers'] = TRUE;

		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open']= '<li class="page-item">';
		$config['num_tag_close']='</li>';
		$config['cur_tag_open'] ="<li class='disabled page-item'><li class='active page-item'>
									<a href='#' class='page-link'>";
		$config['cur_tag_close'] ="<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] ="<li class='page-item'>";
		$config['next_tag1_close'] ="</li>";
		$config['prev_tag_open'] ="<li>";
		$config['prev_tag1_close'] ="<li class='page-item'>";
		$config['first_tag_open'] ="<li>";
		$config['first_tag1_close'] ="<li class='page-item'>";
		$config['last_tag_open'] = "<li>";
		$config['last_tag1_close'] = "<li class='page-item'>";
		$config['attributes'] = array('class' => 'page-link'); 			// pagination links close
		
		$this->pagination->initialize($config);

	$pagination_link = $this->pagination->create_links(); //setup offset/limit after create_links()

		$params['offset'] = $config['per_page'];
		$params['limit'] = ($page*$config['per_page'])-$config['per_page'];

		$queryString = $this->input->get('q');
		$params['queryString'] = $queryString;
		$user = $this->Costmodel->getCostsheet($params); 

		$data['queryString'] = $queryString;
		$data['pagination_link'] = $pagination_link;
		$data['user'] = $user;
		
		$this->load->view('costsheet_dispaly', $data);
	}

		
}