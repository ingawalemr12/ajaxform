<?php

class Costmodel extends CI_Model
{
	public function createSheet($data)
	{
		$this->db->insert('costsheet', $data);
	}

	public function getCostsheet($params=[])
	{	
		if (isset($params['offset']) && isset($params['limit'])) {
			$this->db->limit($params['offset'], $params['limit']);
		}
		
		if (isset($param['queryString'])) {
			$this->db->or_like('Area', trim($param['queryString']) );   // search record
			$this->db->or_like('Carpet_Area', trim($param['queryString']));
		}
		$user = $this->db->get('costsheet')->result_array();
		return $user; // select * from table where Area like '%q';
	}

	public function getCostsheetCount()
	{
		$count = $this->db->count_all_results('costsheet');
		return $count;
	}
}
?>