<?php $this->load->view('header') ?> 

<?php $this->load->view('sidebar') ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('Home/index') ?>">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Cost Sheet</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
	                        	<div style="margin: 12px 0px -22px 21px;" >
	                        	<p ><strong>Customer Name : Xyz</strong></p>
		                    	<p ><strong>Mobile Number : 9856932145</strong></p>
		                    	<p ><strong>Flat : 101</strong></p>
		                    	</div>
		                    
                            <form action="<?php echo base_url() ?>Cost/costCreate" class="form" name="costForm" id="costForm" method="post"  >
                                <div class="card-body">
                                    <h4 class="card-title bg-dark text-white text-center text-uppercase pt-2 pb-2">Costsheet Detionsails & Calculations</h4>
                                    
                                    <div class="form-group row">
                                        <label for="Area" class="col-sm-3 text-right control-label col-form-label">Area</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="area" id="area" placeholder="Area Here" value=""  class="form-control <?php echo (form_error('area') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('area'); ?>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Rate</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="rate" id="lname" placeholder="Rate Here" class="form-control <?php echo (form_error('rate') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('rate'); ?>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="carpet" class="col-sm-3 text-right control-label col-form-label">Carpet Area</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="carpet" id="carpet" placeholder="Carpet Area Here" class="form-control <?php echo (form_error('carpet') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('carpet'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="agrCost" class="col-sm-3 text-right control-label col-form-label">Agr. Cost</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="agrCost" id="agrCost" placeholder="Agr Cost Here"  class="form-control <?php echo (form_error('agrCost') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('agrCost'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mseb" class="col-sm-3 text-right control-label col-form-label">MSEB</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="mseb" id="mseb" placeholder="MSEB Here"  class="form-control <?php echo (form_error('mseb') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('mseb'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="society" class="col-sm-3 text-right control-label col-form-label">Society Formation</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="society" id="society" placeholder="Society Formation Here" class="form-control <?php echo (form_error('society') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('society'); ?>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="club" class="col-sm-3 text-right control-label col-form-label">Club House Charges</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="club" id="club" placeholder="Club House Charges Here" class="form-control <?php echo (form_error('club') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('club'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="stamp" class="col-sm-3 text-right control-label col-form-label">Stamp-Duty</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="stamp" id="stamp" placeholder="Stamp-Duty Here" class="form-control <?php echo (form_error('stamp') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('stamp'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="maintenance" class="col-sm-3 text-right control-label col-form-label">Maintenance</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="maintenance" id="maintenance" placeholder="Stamp-Duty Here" class="form-control <?php echo (form_error('maintenance') !="") ? 'is-invalid' : '' ?>">
                                        <?php echo form_error('maintenance'); ?>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="border-top" style="display: inline;">
                                    <div class="card-body">
                                        <input type="submit" class="btn btn-primary" name="ok">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

<?php $this->load->view('footer') ?> 
