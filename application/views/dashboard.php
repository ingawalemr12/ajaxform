<?php $this->load->view('header') ?> 

<?php $this->load->view('sidebar') ?>

        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('Home/index') ?>">Home</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($this->session->flashdata('success'))){ ?>
                           <div class="alert alert-success"><?php echo $this->session->flashdata('success') ?></div>
                     <?php   }?>
                        <div class="card">
                            <div class="card-body text-center" style="height: 390px;">
                                <h3 style="padding-top: 190px;">
                                    Welcome To Dashboard - Costsheet Details
                                </h3>    
                            </div>                           
                        </div>
                    </div>
                </div>
               
            </div>

<?php $this->load->view('footer') ?>