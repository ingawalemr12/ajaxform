<?php $this->load->view('header') ?> 

<?php $this->load->view('sidebar') ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('Home/index') ?>">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> Display Cost Sheet</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="card">
                  <div class="card-header">
                    <a class="navbar-brand">View Cost Sheet</a>
                      <form id="searchFrm" name="searchFrm" method="get" action="">
                        <div class="input-group mb-0">
                            <input type="text" name="q" value="<?php echo $queryString; ?>" 
                            class="form-control" placeholder="search">
                              <div class="input-group-append">
                                  <button class="input-group-text" id="basic-addonl"> 
                                    <i class="fas fa-search"></i>
                                  </button>
                             </div>
                        </div>
                      </form>
                    
                   </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($this->session->flashdata('success'))) { ?>
                           <div class="alert alert-success">
                               <?php echo $this->session->flashdata('success') ?>
                           </div>
                        <?php } ?>
		                <table class="table table-striped" style="border:2px solid;">
                            <tr style="background-color: red" class="text-white">
                                <th>ID</th>
                                <th>Area</th>
                                <th>Rate</th>
                                <th>Carpet_Area</th>                                
                                <th>Agr_Cost</th>                          
                                <th>MSEB </th>
                                <th>Society_Formation </th>
                                <th>Club_House </th>
                                <th>Stamp_Duty </th>
                                <th>Maintenance </th>
                           <!-- <th>Registration </th>
                                <th>GST </th>
                                <th>Total_Cost </th>                                               
                            -->
                            </tr> 

                            <?php if (!empty($user))  { 
                                foreach ($user as $value) { ?>  
                                    <tr>
                                        <td><?php echo $value['id']; ?></td>
                                        <td><?php echo $value['area']; ?></td>
                                        <td><?php echo $value['rate']; ?></td>
                                        <td><?php echo $value['carpet']; ?></td>
                                        <td><?php echo $value['agrCost']; ?></td>
                                        <td><?php echo $value['mseb']; ?></td>
                                        <td><?php echo $value['society']; ?></td>
                                        <td><?php echo $value['club']; ?></td>
                                        <td><?php echo $value['stamp']; ?></td>
                                        <td><?php echo $value['maintenance']; ?></td>
                                    <!--<td><?php //echo $value['Registration']; ?></td>
                                        <td><?php //echo $value['GST']; ?></td>
                                        <td><?php //echo $value['Total_Cost']; ?></td>
                                    -->
                                    </tr>  
                            <?php   } } ?>
                        </table>
                   </div>
                </div>

                <div>
                    <?php echo $pagination_link; ?>
                </div>
            </div>

<?php $this->load->view('footer') ?> 
