<?php $this->load->view('header') ?> 

<?php $this->load->view('sidebar') ?>

        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('Home/index') ?>">Home</a></li>
                                    <li class="breadcrumb-item"><a href="#">Images</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    Insert New Image
                                </div>
                            </div>
                        <form method="post" enctype="multipart/form-data" 
                        action="<?php echo base_url().'Image/imageSelect' ?>">
                            <div class="card-body">
                                    <div class="form-group">
                                         <label>Name</label>
                                         <input type="text" name="name" value="<?php echo set_value('name') ?>" class="form-control <?php echo(form_error('name') !="") ? 'is-invalid' : '' ?>">
                                         <?php echo form_error('name'); ?>
                                    </div>
                                    <div class="form-group">
                                         <label>Image</label><br>
                                          <input type="file" name="image" id="image"  
                                 class="form-control <?php echo (!empty($erronImage)) ? 'is-invalid' : '' ; ?>" >
                                  <?php echo (!empty($erronImage)) ? $erronImage : '' ; ?>
                                  </div> 
                            </div>
                            <div class="card-footer">
                                <input type="submit" name="Upload" class="btn btn-primary">
                                <a href="<?php echo base_url('Home/index'); ?>" class="btn btn-secondary">Back to Dashboard</a>
                            </div>  
                            
                        </form>                         
                        </div>
                    </div>
                </div>
               
            </div>

<?php $this->load->view('footer') ?>